dt = 0.01
g = 1

speed = 50

function Cialo(x, y, vx, vy, m, color){
	this.x = x;
	this.y = y;
	this.vx = vx;
	this.vy = vy;
	this.m = m;
	this.color = color;
}
Cialo.prototype.wypisz = function () {
	return "("+this.x+", "+this.y+"):"+this.vx+", "+this.vy+", "+this.m;
}
Cialo.prototype.obliczPredkosc = function (cialo){
	var r = Math.sqrt(Math.pow(this.x-cialo.x, 2) + Math.pow(this.y-cialo.y, 2));
	var fx = g*this.m*cialo.m/r/r*Math.sign(cialo.x-this.x);
	var fy = g*this.m*cialo.m/r/r*Math.sign(cialo.y-this.y);
	this.vx += dt*fx/this.m;
	this.vy += dt*fy/this.m;
}	

Cialo.prototype.obliczPozycje = function (){
	this.x += dt*this.vx;
	this.y += dt*this.vy;
}

var ctx;

function clearCanvas(w, h){
	// alert(w)
	ctx.clearRect(0,0,w,h);
}

function drawCialo(cialo){
	ctx.fillStyle = cialo.color;
	ctx.beginPath();
	ctx.arc((cialo.x-kloc.x)/1+w/2, (kloc.y-cialo.y)/1+h/2, Math.max(1, cialo.m/1), 0, 2*Math.PI);
	
	ctx.fill();

}

function epoch(){
	for(var i=0; i<speed; i++){
		ziemia.obliczPredkosc(grzmot);
		ziemia.obliczPredkosc(kloc);
		grzmot.obliczPredkosc(ziemia);
		grzmot.obliczPredkosc(kloc);
		kloc.obliczPredkosc(ziemia);
		kloc.obliczPredkosc(grzmot);
		ziemia.obliczPozycje();
		grzmot.obliczPozycje();
		kloc.obliczPozycje();
	}

	clearCanvas(w, h);
	drawCialo(kloc);
	drawCialo(ziemia);
	drawCialo(grzmot);
}

window.onload=function(){
	ctx = document.getElementById("canvas").getContext("2d");
	w = document.getElementById("canvas").width;
	h = document.getElementById("canvas").height;
	/* Ziemia */
	ziemia = new Cialo(-160, -230, -0.3, 0.1, 0.9, "lime");

	/* Księżyc */
	grzmot = new Cialo(-155, -215, -0.1, -0.04, .0004, "white");

	/* Słońce */
	kloc = new Cialo(0, 0, 0, 0, 25, "#fffc00");

	//clearCanvas(w, h);
	//drawCialo(ziemia);
	//drawCialo(grzmot);
	setInterval(epoch, 1);

}